import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


class Browser(object):
    def __init__(self):
        self.driver = None
        self.wait = None

    def getBrowser(self):
        self.driver = webdriver.Chrome('driver/chromedriver')
        self.driver.maximize_window()
        return self.driver

    def getWait(self):
        self.wait = WebDriverWait(self.driver, 20)
        return self.wait
