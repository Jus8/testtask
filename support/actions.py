import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import *


class Actions:
    def __init__(self, driver):
        self.driver = driver

    def clickElementByClassName(self, className):
        self.driver.find_element_by_class_name(className) \
            .click()

    def clickElementById(self, id):
        self.driver.find_element_by_id(id).click()

    def getTextByClassName(self, className):
        return self.driver.find_element_by_class_name(className).text

    def getTextByXpath(self, xpath):
        return self.driver.find_element_by_xpath(xpath).text

    def getTextById(self, id):
        return self.driver.find_element_by_id(id).text

    def deleteTextByXpath(self, xpath):
        actions = ActionChains(self.driver)
        codeMirror = self.driver.find_element_by_xpath(xpath)
        actions.click(codeMirror).perform()
        actions.send_keys(Keys.LEFT_SHIFT).perform()
        actions.send_keys(Keys.BACKSPACE).perform()

    def inputTextByXpath(self, xpath, text):
        actions = ActionChains(self.driver)
        codeMirror = self.driver.find_element_by_xpath(xpath)
        actions.click(codeMirror).perform()
        actions.send_keys(Keys.LEFT_SHIFT).perform()
        actions.send_keys(text).perform()
