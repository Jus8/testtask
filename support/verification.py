import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


class Verify:
    def __init__(self, driver, wait):
        self.driver = driver
        self.wait = wait

    def waitLoadElementXpath(self, xpath):
        self.wait.until(EC.presence_of_element_located((By.XPATH, xpath)))

    def waitLoadElementById(self, id):
        self.wait.until(EC.presence_of_element_located((By.ID, id)))

    def waitDelElementByClassName(self, classname):
        self.wait.until(EC.invisibility_of_element((By.CLASS_NAME, classname)))

    def waitAndCloseAlert(self):
        alert = self.wait.until(EC.alert_is_present())
        alert.accept()
