import configparser
import os

path = 'config/settings.ini'


class Conf:
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле design.py
        super().__init__()

    def create_config(self, path):
        config = configparser.ConfigParser()
        config.add_section("Check address")
        config.set("Check address", "ContactName", "Giovanni Rovelli")
        config.set("Check address", "Address", "Via Ludovico il Moro 22")
        config.add_section("Update")
        config.set("Update", "CustomerName", "Наждачка Анна")
        config.set("Update", "ContactName", "Ананидзе Жано")
        config.set("Update", "Address", "Улица Юбилейная")
        config.set("Update", "City", "Саранск Сити")
        config.set("Update", "PostalCode", "1234")
        config.set("Update", "Country", "Царство-государство")
        config.add_section("Table_Customers")
        config.set("Table_Customers", "Columns", "CustomerID\nCustomerName\nContactName\nAddress\nCity\nPostalCode\nCountry")
        config.add_section("Table_Products")
        config.set("Table_Products", "Columns", "ProductID\nProductName\nSupplierID\nCategoryID\nUnit\nPrice")
        config.add_section("Table_Categories")
        config.set("Table_Categories", "Columns", "CategoryID\nCategoryName\nDescription")
        config.add_section("Table_Join")
        config.set("Table_Join", "Columns", "ProductID\nProductName\nSupplierID\nCategoryID\nUnit\nPrice\nCategoryName\nDescription")

        with open(path, "w") as config_file:
            config.write(config_file)

    def edit_config(self, section, settings, value):
        config = configparser.ConfigParser()
        config.read(path)
        config.set(section, settings, value)

        with open(path, "w") as config_file:
            config.write(config_file)

    def get_config(self, path):
        if not os.path.exists(path):
            self.create_config(path)

        config = configparser.ConfigParser()
        config.read(path)
        return config

    def get_setting(self, section, setting):
        config = self.get_config(path)
        value = config.get(section, setting)
        return value

    def get_items(self, section):
        config = self.get_config(path)
        item = config.items(section)
        return item
