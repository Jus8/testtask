from driver.driver import Browser
from support.actions import Actions
from support.verification import Verify
from config.setting import Conf
from functions.functions import Func

list_Customers = []

class RunTest():
    def setUp(self):
        self.driver = Browser.getBrowser(self)
        self.wait = Browser.getWait(self)
        self.actions = Actions(self.driver)
        self.verification = Verify(self.driver, self.wait)
        self.setting = Conf()
        self.function = Func(self.driver)

    def precondition(self):
        self.driver.get('https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all')

        # работа с окном куки
        self.verification.waitLoadElementById('accept-choices')
        self.actions.clickElementById('accept-choices')
        self.verification.waitDelElementByClassName('sn-inner')

    def case1(self):
        global list_Customers
        # пишем скрипт и выполняем запрос
        self.function.executeSql('SELECT * FROM Customers;')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # получаем данные из таблицы
        list_Customers = self.function.getDataFromTable('Table_Customers', count)

        # ищем нужную строчку
        for i in list_Customers:
            if i['ContactName'] == self.setting.get_setting('Check address', 'ContactName'):
                num_str = int(i['CustomerID']) - 1

        # проверяем, что адрес совпадает
        if list_Customers[num_str]['Address'] == self.setting.get_setting('Check address', 'Address'):
            print('fine, address is correct')
        else:
            print('Oops! Address is wrong!')

    def case2(self):
        # пишем запрос с выборкой
        self.function.executeSql('SELECT * FROM Customers where city= "London";')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        if count == 6:
            print('number town is true')
        else:
            print('Oops! Number town is wrong!')

    def case3(self):
        # пишем запрос на добавление и выполняем запрос
        self.function.executeSql('insert into Customers ("CustomerID" , "CustomerName" , "ContactName" , '
                                 '"Address" , "City" , "PostalCode" , "Country" ) values ("' + str(
            len(list_Customers) + 1) +
                                 '" , "lalala" , "blahblah" , "street Lenina" , "not London" , "1234" , "not UK");')
        self.verification.waitLoadElementById('iframeResultSQL')

        # пишем запрос селекта для проверки и выполняем запрос
        self.function.executeSql('SELECT * FROM Customers;')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # добавляем новую строку в список
        dict = {}
        j = 0
        while j < len(self.setting.get_setting('Table_Customers', 'Columns').split('\n')):
            dict[self.setting.get_setting('Table_Customers', 'Columns').split('\n')[j]] = self.actions.getTextByXpath(
                '//*[@id="divResultSQL"]/div/table/tbody/tr[' + str(count + 1) + ']/td[' + str(j + 1) + ']')
            j += 1
        list_Customers.append(dict)

        # проверка, что добавлена новая запись
        if count == len(list_Customers):
            print('new recording added')
        else:
            print('Oops! New recording not added!')

    def case4(self):
        # self.actions.inputTextByXpath('//*[@id="tryitform"]/div/div[6]/div[1]/div/div/div/div[5]/pre[1]/span/span[1]',
        #                               'update Customers set CustomerName= "0", ContactName= "1", Address= "2", City= "3", PostalCode= "4", Country= "5" where "CustomerID"="92";')

        # пишем запросы на обновление и выполняем (сделала циклом, чтобы не завязываться на количестве. пример единого запроса выше)
        i = 0
        while i != len(self.setting.get_items('Update')):
            self.function.executeSql('update Customers set ' + self.setting.get_items('Update')[i][0] + '= "' +
                                     self.setting.get_items('Update')[i][1] + '" where CustomerID="92";')
            self.verification.waitLoadElementById('iframeResultSQL')
            i += 1

        # пишем запрос селекта для проверки и выполняем запрос
        self.function.executeSql('SELECT * FROM Customers;')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # обновляем данные в списке
        j = 1
        while j < len(self.setting.get_setting('Table_Customers', 'Columns').split('\n')):
            list_Customers[91][
                self.setting.get_setting('Table_Customers', 'Columns').split('\n')[j]] = self.actions.getTextByXpath(
                '//*[@id="divResultSQL"]/div/table/tbody/tr[' + str(count + 1) + ']/td[' + str(j + 1) + ']')
            j += 1

        # сравниваем данные
        if count == len(list_Customers):
            if list_Customers[91]['CustomerName'] == self.setting.get_setting('Update', 'CustomerName') and \
                    list_Customers[91]['ContactName'] == self.setting.get_setting('Update', 'ContactName') and \
                    list_Customers[91]['Address'] == self.setting.get_setting('Update', 'Address') and \
                    list_Customers[91]['City'] == self.setting.get_setting('Update', 'City') and \
                    list_Customers[91]['PostalCode'] == self.setting.get_setting('Update', 'PostalCode') and \
                    list_Customers[91]['Country'] == self.setting.get_setting('Update', 'Country'):
                print('good, data is update')
            else:
                print('Oops! Data isnt update')
        else:
            print('Oops! String number are different!')

    def case5(self):
        # пишем скрипт и выполняем запрос
        self.function.executeSql('SELECT * FROM Products where categoryid = "7";')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # получаем данные из таблицы
        list_Products = self.function.getDataFromTable('Table_Products', count)

        # пишем скрипт и выполняем запрос
        self.function.executeSql('SELECT * FROM Categories where categoryid = "7";')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # получаем данные из таблицы
        list_Categories = self.function.getDataFromTable('Table_Categories', count)

        # пишем скрипт join и выполняем запрос
        self.function.executeSql('SELECT * FROM Products as p '
                                 'left join Categories as c on p.categoryid = c.categoryid '
                                 'where c.CategoryName = "Produce"')
        self.verification.waitLoadElementXpath('//*[@id="divResultSQL"]/div/table/tbody/tr[1]/th[1]')

        # смотрим сколько строк в таблице
        count = self.function.stringNumber()

        # получаем данные из таблицы
        list_Join = self.function.getDataFromTable('Table_Join', count)

        # сравниваем значения
        self.function.compareJoin(list_Join, list_Products, list_Categories)

        # пишем скрипт и выполняем запрос
        self.function.executeSql(
            'Расцветали яблони и груши\nПоплыли туманы над рекой\nВыходила на берег Катюша\nНа высокий берег, на крутой')
        self.verification.waitAndCloseAlert()
        self.verification.waitLoadElementById('resultSQL')

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    r = RunTest()
    r.setUp()
    r.precondition()
    r.case1()
    r.case2()
    r.case3()
    r.case4()
    r.case5()
    r.tearDown()
