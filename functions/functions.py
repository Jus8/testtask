from driver.driver import Browser
from support.actions import Actions
from support.verification import Verify
from config.setting import Conf
import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

class Func:
    def __init__(self, driver):
        self.driver = driver
        self.wait = Browser.getWait(self)
        self.actions = Actions(self.driver)
        self.verification = Verify(self.driver, self.wait)
        self.setting = Conf()

    def stringNumber(self):
        count = self.actions.getTextByXpath('//*[@id="divResultSQL"]/div/table/tbody')
        count = len(count.split('\n')) - 1
        return count

    def getDataFromTable(self, table, count):
        data_list = []
        i = 0
        while i < count:
            dict = {}
            j = 0
            while j < len(self.setting.get_setting(table, 'Columns').split('\n')):
                dict[self.setting.get_setting(table, 'Columns').split('\n')[j]] = self.actions.getTextByXpath(
                    '//*[@id="divResultSQL"]/div/table/tbody/tr[' + str(i + 2) + ']/td[' + str(j + 1) + ']')
                j += 1
            data_list.append(dict)
            i += 1
        return data_list

    def executeSql(self, sql):
        self.actions.deleteTextByXpath(
            '//*[@id="tryitform"]/div/div[6]/div[1]/div/div/div/div[5]/pre[1]/span/span[' + str(len([
                x for x in self.actions.getTextByClassName('CodeMirror-line').split(' ') if x != ''])) + ']')
        self.actions.inputTextByXpath('//*[@id="tryitform"]/div/div[6]/div[1]/div/div/div/div[5]/pre[1]/span/span[1]',
                                      sql)
        self.actions.clickElementByClassName('w3-green.w3-btn')

    def compareJoin(self, list_Join, list_Products, list_Categories):
        if len(list_Join) == len(list_Products) and len(list_Join[0]) == (
                len(list_Products[0]) + len(list_Categories[0]) - 1):
            i = 0
            while i < (len(list_Products) - 1):
                for k in list_Join:
                    if k['CategoryID'] == list_Categories[0]['CategoryID'] and \
                            k['CategoryName'] == list_Categories[0]['CategoryName'] and \
                            k['Description'] == list_Categories[0]['Description']:
                        for m in list_Products:
                            if k['Unit'] == m['Unit']:
                                if k['ProductID'] == m['ProductID'] and \
                                        k['ProductName'] == m['ProductName'] and \
                                        k['SupplierID'] == m['SupplierID'] and \
                                        k['Price'] == m['Price']:
                                    print('string for %s are matches' % k['Unit'])
                                    i += 1
                    else:
                        print('Oops! String for %s are different!' % k['Unit'])
